﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CommonTestsUtils.Abstractions;
using CommonUtils.Common.Logger;
using ConcurrentCollections;
using Moq;
using NUnit.Framework;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TNotifications.Interfaces;
using TNotifications.Services;

namespace TNotifications.Tests.ServicesTests
{
    [TestFixture]
    public class TelegramServiceTests : TestWithLogger
    {
        private class TestTelegramService : TelegramService
        {
            public TestTelegramService(ITelegramBotClient tBotClient, ILogger logger) : base(tBotClient, logger)
            {
            }

            public ConcurrentHashSet<long> SupportedChats => SupportedChatIds;
        }
         

        private Mock<ITelegramBotClient> botClient;
        private TestTelegramService telegramService;
        
        [SetUp]
        public void Setup()
        {
            botClient = GetTelegramBotClient();
            telegramService = new TestTelegramService(botClient.Object, logger);
        }

        private Mock<ITelegramBotClient> GetTelegramBotClient()
        {
            var client = new Mock<ITelegramBotClient>();

            client.Setup(o =>
                    o.SendTextMessageAsync(It.IsAny<ChatId>(), It.IsAny<string>(), ParseMode.Default, false, false, 0,
                        null, default(CancellationToken)))
                .Returns(() => null);

            return client;
        }

        [Test]
        public void ConstructorTest()
        {
            Assert.Throws<ArgumentNullException>(() => new TelegramService(null, logger));
            Assert.Throws<ArgumentNullException>(() => new TelegramService(botClient.Object, null));
            Assert.IsInstanceOf<ITelegramService>(telegramService);
        }

        [Test]
        public void AddChatTest()
        {
            long newChat = -1235;
            telegramService.AddChat(newChat);

            Assert.AreEqual(1, telegramService.SupportedChats.Count);
            Assert.IsTrue(telegramService.SupportedChats.Contains(newChat));
        }

        [Test]
        public void SupportsChatTest()
        {
            long existedChat = -12345;
            long notExistedChat = -12346;

            telegramService.SupportedChats.Add(existedChat);

            Assert.IsTrue(telegramService.SupportsChat(existedChat));
            Assert.IsFalse(telegramService.SupportsChat(notExistedChat));
        }

        [Test]
        public void GetSupportedChatsTest()
        {
            Assert.AreEqual(0, telegramService.GetSupportedChats().Count);
            const int chatsCount = 10;
            var chats = GetChats(chatsCount);
            chats.ForEach(chat => telegramService.SupportedChats.Add(chat));

            var supportedChats = telegramService.GetSupportedChats();
            Assert.AreEqual(chatsCount, supportedChats.Count);
            chats.ForEach(chat => Assert.IsTrue(supportedChats.Contains(chat)));
        }

        [Test]
        public void SendMessageToChatTest()
        {
            var chatId = 12345;
            var message = "Message";

            telegramService.SendMessageToChat(chatId, message);
            VerifySendMessageNever(chatId, message);

            telegramService.SupportedChats.Add(chatId);
            telegramService.SendMessageToChat(chatId, message);
            VerifySendMessageOnce(chatId, message);
        }


        [Test]
        public void SendMessageToAllChatsTest()
        {
            var chats = GetChats(10);
            var message = "message";

            telegramService.SendMessageToAllChats(message);

            VerifySendAnyMessageTimes(Times.Never());

            chats.ForEach(chat => telegramService.SupportedChats.Add(chat));

            telegramService.SendMessageToAllChats(message);

            chats.ForEach(chat => VerifySendMessageOnce(chat, message));
        }

        private void VerifySendAnyMessageTimes(Times times)
        {
            botClient.Verify(
                o => o.SendTextMessageAsync(It.IsAny<ChatId>(), It.IsAny<string>(), ParseMode.Default, false,
                    false, 0, null, default(CancellationToken)), times);

        }

        private void VerifySendMessageOnce(long chatId, string message)
        {
            botClient.Verify(
                o => o.SendTextMessageAsync(It.Is<ChatId>(chat => chat.Identifier == chatId), message, ParseMode.Markdown, false,
                    false, 0, null, default(CancellationToken)), Times.Once);
        }

        private void VerifySendMessageNever(long chatId, string message)
        {
            botClient.Verify(
                o => o.SendTextMessageAsync(It.Is<ChatId>(chat => chat.Identifier == chatId), message, ParseMode.Markdown, false,
                    false, 0, null, default(CancellationToken)), Times.Never);
        }

        private List<long> GetChats(int n)
        {
            var chats = new List<long>();
            for (var i = 1; i <= n; i++)
            {
                chats.Add(-i * 1000);
            }

            return chats;
        }
    }
}