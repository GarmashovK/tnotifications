﻿using System;
using CommonTestsUtils.Abstractions;
using Moq;
using NUnit.Framework;
using TNotifications.Domain.Messages;
using TNotifications.Interfaces;
using TNotifications.Services;

namespace TNotifications.Tests.ServicesTests
{
    [TestFixture]
    public class NotifyServiceTests : TestWithLogger
    {
        private Mock<ITelegramService> telegramService;
        private Mock<IEmailService> emailService;

        private NotifyService notifyService;

        [SetUp]
        public void Setup()
        {
            telegramService = GetTelegramClient();
            emailService = GetEmailService();
            notifyService = new NotifyService(
                emailService.Object,
                telegramService.Object,
                logger);
        }

        private Mock<IEmailService> GetEmailService()
        {
            var emailService = new Mock<IEmailService>();

            emailService.Setup(o => o.SendEmailMessage(It.IsAny<EmailMessage>()))
                .Verifiable();

            return emailService;
        }

        private Mock<ITelegramService> GetTelegramClient()
        {
            var telClient = new Mock<ITelegramService>();

            telClient.Setup(o => o.SendMessageToAllChats(It.IsAny<string>()))
                .Verifiable();

            return telClient;
        }

        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() =>
                new NotifyService(null, telegramService.Object, logger));
            Assert.DoesNotThrow(() =>
                new NotifyService(emailService.Object, null, logger));
            Assert.Throws<ArgumentNullException>(() =>
                new NotifyService(emailService.Object, telegramService.Object, null));

            Assert.IsInstanceOf<INotifyService>(notifyService);
        }

        [Test]
        public void PushMessageTest()
        {
            var telegramChatMessage = new TelegramChatMessage(12345, "ChatTextMessage");
            var telegramNotifyMessage = new TelegramNotifyMessage("NotifyMessage");
            var emailMessage = new EmailMessage("address@ad.com", "subject", "body");
            telegramService.Setup(o => o.SendMessageToAllChats(It.IsAny<string>())).Verifiable();
            telegramService.Setup(o => o.SendMessageToChat(It.IsAny<long>(), It.IsAny<string>())).Verifiable();
            emailService.Setup(o => o.SendEmailMessage(It.IsAny<EmailMessage>())).Verifiable();

            notifyService.PushMessage(telegramChatMessage);
            telegramService.Verify(o => o.SendMessageToChat(telegramChatMessage.ChatId, telegramChatMessage.TextMessage), Times.Once);

            notifyService.PushMessage(telegramNotifyMessage);
            telegramService.Verify(o => o.SendMessageToAllChats(telegramNotifyMessage.TextMessage), Times.Once);

            notifyService.PushMessage(emailMessage);
            emailService.Verify(o => o.SendEmailMessage(emailMessage), Times.Once);
        }

        [Test]
        public void PushMessageTest_DoNothingForNullReference()
        {
            var telegramChatMessage = new TelegramChatMessage(12345, "ChatTextMessage");
            var telegramNotifyMessage = new TelegramNotifyMessage("NotifyMessage");
            var emailMessage = new EmailMessage("address@ad.com", "subject", "body");

            telegramService.Setup(o => o.SendMessageToAllChats(It.IsAny<string>())).Verifiable();
            telegramService.Setup(o => o.SendMessageToChat(It.IsAny<long>(), It.IsAny<string>())).Verifiable();
            emailService.Setup(o => o.SendEmailMessage(It.IsAny<EmailMessage>())).Verifiable();


            notifyService = new NotifyService(null, null, logger);
            notifyService.PushMessage(telegramChatMessage);
            telegramService.Verify(o => o.SendMessageToChat(It.IsAny<long>(), It.IsAny<string>()), Times.Never);

            notifyService.PushMessage(telegramNotifyMessage);
            telegramService.Verify(o => o.SendMessageToChat(It.IsAny<long>(), It.IsAny<string>()), Times.Never);

            notifyService.PushMessage(emailMessage);
            notifyService = new NotifyService(null, telegramService.Object, logger);
            emailService.Verify(o => o.SendEmailMessage(emailMessage), Times.Never);

        }
    }
}