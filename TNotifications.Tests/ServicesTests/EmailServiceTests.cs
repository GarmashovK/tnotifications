﻿using System;
using CommonTestsUtils.Abstractions;
using NUnit.Framework;
using TNotifications.Domain;
using TNotifications.Domain.Messages;
using TNotifications.Interfaces;
using TNotifications.Services;

namespace TNotifications.Tests.ServicesTests
{
    [TestFixture]
    public class EmailServiceTests : TestWithLogger
    {
        private SmtpSettings smtpSettings = new SmtpSettings("Host", 1, "USERNAME", "LOGIN", "password");
        private EmailService emailService;

        [SetUp]
        public void Setup()
        {
            emailService = new EmailService(smtpSettings, logger);
        }

        [Test]
        public void ConstructorTest()
        {
            Assert.Throws<ArgumentNullException>(() => new EmailService(null, logger));
            Assert.Throws<ArgumentNullException>(() => new EmailService(smtpSettings, null));

            Assert.IsInstanceOf<IEmailService>(emailService);
        }

        [Test]
        public void SendEmailMessageTest_Error()
        {
            var emailMsg = new EmailMessage("Address", "Subject", "Body");

            var result = emailService.SendEmailMessage(emailMsg);

            Assert.IsTrue(result.IsFailed());
        }
    }
}