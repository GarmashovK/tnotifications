﻿using Moq;
using TNotifications.Domain.Messages;
using TNotifications.Interfaces;

namespace TNotifications.Tests.Help
{
    public static class ServicesFactory
    {
        public static Mock<INotifyService> GetNotifyService()
        {
            var service = new Mock<INotifyService>();

            service.Setup(o => o.PushMessage(It.IsAny<IMessage>())).Verifiable();

            return service;
        }
    }
}