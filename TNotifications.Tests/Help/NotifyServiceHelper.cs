﻿using Moq;
using TNotifications.Domain.Messages;
using TNotifications.Interfaces;

namespace TNotifications.Tests.Help
{
    public static class NotifyServiceHelper
    {
        public static void VerifyChatTelegramMessage(this Mock<INotifyService> notifyService, long chatId, string expectMsg)
        {
            notifyService.Verify(o => o.PushMessage(It.Is<IMessage>(message => CheckMessage(chatId, expectMsg, message))), Times.Once);
        }
        
        private static bool CheckMessage(long chatId, string expectMsg, IMessage message)
        {
            if (message is TelegramChatMessage)
            {
                var telMessage = message as TelegramChatMessage;
                return telMessage.ChatId == chatId && telMessage.TextMessage == expectMsg;
            }

            return false;
        }
    }
}