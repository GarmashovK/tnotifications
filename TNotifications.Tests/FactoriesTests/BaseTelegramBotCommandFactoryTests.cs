﻿using System;
using System.Collections.Generic;
using CommonTestsUtils.Abstractions;
using CommonUtils.Common.Logger;
using Moq;
using NUnit.Framework;
using TNotifications.Commands.Telegram;
using TNotifications.Factories;
using TNotifications.Interfaces;

namespace TNotifications.Tests.FactoriesTests
{
    [TestFixture]
    public class BaseTelegramBotCommandFactoryTests : TestWithLogger
    {
        public class TestFactory : BaseTelegramBotCommandFactory
        {
            public TestFactory(INotifyService notifyService, ILogger logger) : base(notifyService, logger)
            {
                UpdateCommands();
            }

            protected override IEnumerable<BaseTelegramBotCommand> GetMainCommands()
            {
                return new List<BaseTelegramBotCommand>();
            }

            public new Tuple<string, string> ParseQuery(string query) => base.ParseQuery(query);
        }

        private TestFactory factory;

        [Test]
        public void ConstructorTest()
        {
            var notifyService = new Mock<INotifyService>();
            Assert.Throws<ArgumentNullException>(() => new TestFactory(null, logger));
            Assert.Throws<ArgumentNullException>(() => new TestFactory(notifyService.Object, null));
            factory = new TestFactory(notifyService.Object, logger);
            Assert.IsInstanceOf<ITelegramBotCommandFactory>(factory);
        }
        [Test]
        public void GetBotCommandTest()
        {
            var chatId = 12345;

            Assert.Throws<ArgumentNullException>(() => factory.GetBotCommand(chatId, null));
            Assert.Throws<ArgumentNullException>(() => factory.GetBotCommand(chatId, "     "));

            Assert.IsInstanceOf<HelpBotCommand>(factory.GetBotCommand(chatId, "/help"));
            Assert.IsInstanceOf<NotFoundCommand>(factory.GetBotCommand(chatId, "NotFoundCommand blabla"));
        }

        [Test]
        public void ParseCommandLineTest_WithoutParams()
        {
            var query = "/help";

            var parseResult = factory.ParseQuery(query);

            Assert.AreEqual("/help", parseResult.Item1);
            Assert.AreEqual("", parseResult.Item2);
        }

        [Test]
        public void ParseCommandLineTest_Error()
        {
            string query = "notfoundcommandquery";
            var parseResult = factory.ParseQuery(query);

            Assert.AreEqual("NotFoundCommand", parseResult.Item1);
            Assert.AreEqual(query, parseResult.Item2);
        }

    }
}