﻿using System;
using System.Net;
using NUnit.Framework;
using TNotifications.Domain;

namespace TNotifications.Tests.DomainTests
{
    [TestFixture()]
    public class SmtpSettingsTest
    {
        [Test]
        public void ConstructorTest()
        {
            var username = "username@username.com";
            Assert.Throws<ArgumentNullException>(() => new SmtpSettings(null, 1, username, "login", "password"));
            Assert.Throws<ArgumentException>(() => new SmtpSettings("Host", 0, username, "login", "password"));
            Assert.Throws<ArgumentNullException>(() => new SmtpSettings("Host", 1, username, null, "password"));
            Assert.Throws<ArgumentNullException>(() => new SmtpSettings("Host", 1, username, "login", null));

            Assert.DoesNotThrow(() => new SmtpSettings("Host", 1, username, "login", "password"));
        }

        [Test]
        public void GetSmtpClientTest()
        {
            var username = "username@username.com";
            var settings = new SmtpSettings("Host", 465, username, "login", "password");

            var smtpClient = settings.GetSmtpClient();

            Assert.AreEqual(settings.Host, smtpClient.Host);
            Assert.AreEqual(settings.Port, smtpClient.Port);
            Assert.IsInstanceOf<NetworkCredential>(smtpClient.Credentials);
            var credentials = (NetworkCredential)smtpClient.Credentials;

            Assert.AreEqual(settings.Login, credentials.UserName);
            Assert.AreEqual(settings.Password, credentials.Password);
        }
    }
}