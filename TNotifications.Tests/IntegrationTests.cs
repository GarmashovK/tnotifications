﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CommonTestsUtils.Abstractions;
using CommonUtils.Common.Logger;
using NUnit.Framework;
using Telegram.Bot;
using TNotifications.Bots;
using TNotifications.Commands.Telegram;
using TNotifications.Factories;
using TNotifications.Interfaces;
using TNotifications.Services;

namespace TNotifications.Tests
{
    [TestFixture]
    public class IntegrationTests : TestWithLogger
    {
        private class CommandFactory : BaseTelegramBotCommandFactory
        {
            public CommandFactory(INotifyService notifyService, ILogger logger) : base(notifyService, logger)
            {
            }

            protected override IEnumerable<BaseTelegramBotCommand> GetMainCommands()
            {
                return new List<BaseTelegramBotCommand>();
            }
        }

        private TelegramService telegramService;
        private TelegramBotClient botClient;
        private CommandFactory commandFactory;
        private NotifyService notifyService;
        private HostedTelegramBot hostedTelegramBot;

        [SetUp]
        public void Setup()
        {
            botClient = new TelegramBotClient("");
            telegramService = new TelegramService(botClient, logger);
            telegramService.AddChat(-382098493);
            notifyService = new NotifyService(null, telegramService, logger);
            commandFactory = new CommandFactory(notifyService,logger);
            hostedTelegramBot = new HostedTelegramBot(botClient, telegramService, commandFactory, logger);
        }

        [Test]
        [Ignore("LifeTest")]
        public void RecieveTest()
        {
            Task.Run(() => { hostedTelegramBot.StartReceiving(); });
            while (true)
            {
                Thread.Sleep(10000);
            }
        }
    }
}