﻿using System;
using CommonTestsUtils.Abstractions;
using CommonUtils.Common.Logger;
using Moq;
using NUnit.Framework;
using Telegram.Bot;
using TNotifications.Bots;
using TNotifications.Commands;
using TNotifications.Interfaces;

namespace TNotifications.Tests.BotsTests
{
    [TestFixture]
    public class HostedTelegramBotTests : TestWithLogger
    {
        private Mock<ITelegramBotClient> botClient;
        private Mock<ITelegramService> telegramService;
        private Mock<ITelegramBotCommandFactory> commandFactory;
        private const long chatId = -12345;

        private HostedTelegramBot telegramBot;

        private class TestHostedTelegramBot : HostedTelegramBot
        {
            public TestHostedTelegramBot(ITelegramBotClient client, ITelegramService telegramService,
                ITelegramBotCommandFactory commandFactory, ILogger logger) : base(client, telegramService,
                commandFactory, logger)
            {
            }

            public void DoTestCommand(long chatId, string query) => DoCommand(chatId, query);
        }

        [SetUp]
        public void Setup()
        {
            botClient = GetTelegramClient();
            telegramService = GetTelegramService();
            commandFactory = new Mock<ITelegramBotCommandFactory>();
            telegramBot =
                new HostedTelegramBot(botClient.Object, telegramService.Object, commandFactory.Object, logger);
        }

        private Mock<ITelegramService> GetTelegramService()
        {
            var telService = new Mock<ITelegramService>();

            return telService;
        }

        private Mock<ITelegramBotClient> GetTelegramClient()
        {
            var client = new Mock<ITelegramBotClient>();

            return client;
        }

        [Test]
        public void ConstructorTest()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new HostedTelegramBot(null, telegramService.Object, commandFactory.Object, logger));
            Assert.Throws<ArgumentNullException>(() =>
                new HostedTelegramBot(botClient.Object, null, commandFactory.Object, logger));
            Assert.Throws<ArgumentNullException>(() =>
                new HostedTelegramBot(botClient.Object, telegramService.Object, null, logger));
            Assert.Throws<ArgumentNullException>(() =>
                new HostedTelegramBot(botClient.Object, telegramService.Object, commandFactory.Object, null));

            Assert.IsInstanceOf<IHostedTelegramBot>(telegramBot);
        }

        [Test]
        public void DoCommandTest()
        {
            var testTelegramBot =
                new TestHostedTelegramBot(botClient.Object, telegramService.Object, commandFactory.Object, logger);
            var executedCommand = false;
            var chatId = 12345;
            var query = "/commandName";
            var command = new BotCommand(query, () => executedCommand = true);
            telegramService.Setup(o => o.SupportsChat(chatId)).Returns(false);

            testTelegramBot.DoTestCommand(chatId, query);
            Assert.IsFalse(executedCommand);

            telegramService.Setup(o => o.SupportsChat(chatId)).Returns(true);
            commandFactory.Setup(o => o.GetBotCommand(chatId, query)).Returns((BotCommand) null);

            testTelegramBot.DoTestCommand(chatId, query);
            Assert.IsFalse(executedCommand);

            telegramService.Setup(o => o.SupportsChat(chatId)).Returns(true);
            commandFactory.Setup(o => o.GetBotCommand(chatId, query)).Returns(command);
            testTelegramBot.DoTestCommand(chatId, query);
            Assert.IsTrue(executedCommand);
        }

        [Test]
        public void DoCommandTest_MultipleQueries()
        {
            var testTelegramBot =
                new TestHostedTelegramBot(botClient.Object, telegramService.Object, commandFactory.Object, logger);
            var executedCount = 0;
            var chatId = 12345;
            var query = "/commandName";
            var queries = "/commandName\n/commandName\n/commandName";
            var command = new BotCommand(query, () => executedCount++);
            telegramService.Setup(o => o.SupportsChat(chatId)).Returns(true);
            commandFactory.Setup(o => o.GetBotCommand(chatId, query)).Returns(command);

            testTelegramBot.DoTestCommand(chatId, queries);

            Assert.AreEqual(3, executedCount);
        }
    }
}