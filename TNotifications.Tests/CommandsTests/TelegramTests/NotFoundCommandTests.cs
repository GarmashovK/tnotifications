﻿using CommonTestsUtils.Abstractions;
using Moq;
using NUnit.Framework;
using TNotifications.Commands.Telegram;
using TNotifications.Interfaces;
using TNotifications.Tests.Help;

namespace TNotifications.Tests.CommandsTests.TelegramTests
{
    [TestFixture]
    public class NotFoundCommandTests : TestWithLogger
    {
        private NotFoundCommand notFoundCommand;
        private Mock<INotifyService> notifyService;

        [SetUp]
        public void Setup()
        {
            notifyService = ServicesFactory.GetNotifyService();
            notFoundCommand = new NotFoundCommand(notifyService.Object, logger);
        }


        [Test]
        public void ConstructorTest()
        {
            Assert.IsInstanceOf<BaseTelegramBotCommand>(notFoundCommand);
            Assert.AreEqual("NotFoundCommand", notFoundCommand.CommandName);
        }

        [Test]
        public void DoOperationTest()
        {
            var chatId = 1234;
            var command = "TheNotFoundCommand";
            notFoundCommand.SetRequestValues(chatId, command);
            notFoundCommand.Execute();
            //notifyService.VerifyChatTelegramMessage(chatId, $"Неизвестная команда: {command}.");
        }
    }
    
}