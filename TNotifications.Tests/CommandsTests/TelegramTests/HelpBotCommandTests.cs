﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonTestsUtils.Abstractions;
using CommonUtils.Common.Logger;
using Moq;
using NUnit.Framework;
using TNotifications.Commands.Telegram;
using TNotifications.Domain.Messages;
using TNotifications.Interfaces;
using TNotifications.Tests.Help;

namespace TNotifications.Tests.CommandsTests.TelegramTests
{
    [TestFixture]
    public class HelpBotCommandTests : TestWithLogger
    {
        private class TestHelpBotCommand : HelpBotCommand
        {

            public string GetHelpString()
            {
                return GetHelp();
            }

            public TestHelpBotCommand(INotifyService notifyService, ILogger logger) : base(notifyService, logger)
            {
            }
        }

        private class TestCommand : BaseTelegramBotCommand
        {
            private string desc = "";
            public TestCommand(string commandName, INotifyService notifyService, ILogger logger) : base(commandName, notifyService, logger)
            {
            }

            public void SetDescription(string desc)
            {
                this.desc = desc;
            }

            protected override string GetDescription()
            {
                return desc;
            }

            protected override void CustomOperation()
            {
                throw new NotImplementedException();
            }
        }

        private Mock<INotifyService> notifyService;
        private TestHelpBotCommand helpCommand;

        [SetUp]
        public void Setup()
        {
            notifyService = ServicesFactory.GetNotifyService();
            helpCommand = new TestHelpBotCommand(notifyService.Object, logger);
        }

        [Test]
        public void ConstructorTest()
        {
            Assert.AreEqual("/help", helpCommand.CommandName);
            Assert.IsInstanceOf<BaseTelegramBotCommand>(helpCommand);
        }

        [Test]
        public void DoOperationTest()
        {
            var chatId = 12345;
            var otherChatId = 12346;
            helpCommand.SetRequestValues(chatId, null).Execute();
            notifyService.Verify(o => o.PushMessage(It.IsAny<TelegramChatMessage>()), Times.Once);

            helpCommand.SetRequestValues(otherChatId, null).Execute();
            notifyService.Verify(o => o.PushMessage(It.IsAny<TelegramChatMessage>()), Times.Exactly(2));
        }

        [Test]
        public void SetSupportedCommandsDescriptionsTest()
        {
            var botCommands = GetBotCommands();

            helpCommand.SetSupportedCommandsDescriptions(botCommands);

            var helpString = helpCommand.GetHelpString();
            var helpLines = helpString
                .Split(Environment.NewLine)
                .Skip(1)
                .ToArray();
            for (var i = 0; i < botCommands.Count; i++)
                Assert.AreEqual(botCommands[i].Description, helpLines[i]);
        }

        private List<BaseTelegramBotCommand> GetBotCommands(int n = 10)
        {
            var commands = new List<BaseTelegramBotCommand>();
            for (var i = 0; i < n; i++)
                commands.Add(GetNewBotCommand(i));
            return commands;
        }

        public BaseTelegramBotCommand GetNewBotCommand(int i)
        {
            var descr = $"Description{Guid.NewGuid()}";
            var command = new TestCommand($"Command{i}", notifyService.Object, logger);
            command.SetDescription(descr);

            return command;
        }
    }
}