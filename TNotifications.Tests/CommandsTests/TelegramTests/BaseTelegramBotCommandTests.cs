﻿using System;
using CommonTestsUtils.Abstractions;
using CommonUtils.Common.Logger;
using Moq;
using NUnit.Framework;
using TNotifications.Commands;
using TNotifications.Commands.Telegram;
using TNotifications.Domain.Messages;
using TNotifications.Interfaces;
using TNotifications.Tests.Help;

namespace TNotifications.Tests.CommandsTests.TelegramTests
{
    public class BaseTelegramBotCommandTests : TestWithLogger
    {

        private class TestTelegramBotCommand : BaseTelegramBotCommand
        {
            public Action action => base.CommandAction;
            protected override string GetDescription()
            {
                return "Description";
            }

            protected override void CustomOperation()
            {
                notifyService.PushMessage(new TelegramChatMessage(ChatId, Parameters));
            }

            protected override void ParseParameters(string parameters)
            {
            }

            public string[] GetSlittedParamateres() => GetSplittedParameters();

            public TestTelegramBotCommand(string commandName, INotifyService notifyService, ILogger logger)
                : base(commandName, notifyService, logger)
            {
            }
        }

        private Mock<INotifyService> notifyService;
        private TestTelegramBotCommand telegramCommand;

        [SetUp]
        public void Setup()
        {
            notifyService = ServicesFactory.GetNotifyService();
            telegramCommand = new TestTelegramBotCommand("commandName", notifyService.Object, logger);
        }

        [Test]
        [Ignore("Ignore")]
        public void GetSplittedParametersTest()
        {
            var chatId = 1234;
            var pars = new[]
            {
                "param1", "param2", "3456.f43432"
            };
            var parameters = $"{pars[0]}   {pars[1]}    {pars[2]}";
            telegramCommand.SetRequestValues(chatId, parameters);

            var splitted = telegramCommand.GetSlittedParamateres();

            //splitted.Should().BeEquivalentTo(pars);
        }

        [Test]
        public void ConstructorTest()
        {
            var commandName = "CommandName";
            Assert.Throws<ArgumentNullException>(() =>
                new TestTelegramBotCommand(null, notifyService.Object, logger));
            Assert.Throws<ArgumentNullException>(() =>
                new TestTelegramBotCommand(commandName, null, logger));
            Assert.Throws<ArgumentNullException>(() =>
                new TestTelegramBotCommand(commandName, notifyService.Object, null));

            telegramCommand = new TestTelegramBotCommand(commandName, notifyService.Object, logger);
            Assert.AreEqual(commandName, telegramCommand.CommandName);
            Assert.IsInstanceOf<BotCommand>(telegramCommand);
        }

        [Test]
        public void ExecuteTest()
        {
            long chatId = 12345;
            var parameters = "Parameters";

            telegramCommand.SetRequestValues(chatId, parameters);

            telegramCommand.Execute();
            notifyService.Verify(o => o.PushMessage(It.IsAny<TelegramChatMessage>()), Times.Once);

            var otherChatId = 54321;
            var otherParameters = "OtherParamters";

            notifyService.Invocations.Clear();
            telegramCommand.SetRequestValues(otherChatId, otherParameters);
            telegramCommand.Execute();
            notifyService.Verify(o => o.PushMessage(It.IsAny<TelegramChatMessage>()), Times.Once);
        }

        [Test]
        public void GetBotCommandTest()
        {
            var botCommand = telegramCommand.GetBotCommand();

            Assert.AreNotEqual(botCommand, telegramCommand);
            Assert.AreEqual(telegramCommand.CommandName, botCommand.CommandName);
        }
    }
}