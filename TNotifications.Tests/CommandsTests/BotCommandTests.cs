using System;
using NUnit.Framework;
using TNotifications.Commands;

namespace TNotifications.Tests.CommandsTests
{
    [TestFixture]
    public class BotCommandTests
    {
        [Test]
        public void ConstructorTest()
        {
            Assert.Throws<ArgumentNullException>(() => new BotCommand(null, () =>{}));
            Assert.Throws<ArgumentNullException>(() => new BotCommand("CommandName", null));

            Assert.DoesNotThrow(() => new BotCommand("CommandName", () => { }));
        }

        [Test]
        public void ExecuteTest()
        {
            var executed = false;
            Action action = () => { executed = true; };
            var botCommand = new BotCommand("CommandName", action);

            Assert.IsFalse(executed);
            botCommand.Execute();
            Assert.IsTrue(executed);
        }
    }
}