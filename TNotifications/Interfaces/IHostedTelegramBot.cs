﻿namespace TNotifications.Interfaces
{
    public interface IHostedTelegramBot
    {
        void StartReceiving();
        void StopReceiving();
    }
}