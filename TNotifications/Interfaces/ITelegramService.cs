﻿using System.Collections.Generic;

namespace TNotifications.Interfaces
{
    public interface ITelegramService
    {
        void AddChat(long chatId);
        bool SupportsChat(long chatId);
        IReadOnlyList<long> GetSupportedChats();
        void SendMessageToChat(long chat, string message);
        void SendMessageToAllChats(string message);
    }
}