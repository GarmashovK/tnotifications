﻿using CommonUtils.Common.Results;
using TNotifications.Domain.Messages;

namespace TNotifications.Interfaces
{
    public interface IEmailService
    {
        Result SendEmailMessage(EmailMessage emailMessage);
    }
}