﻿using TNotifications.Commands;

namespace TNotifications.Interfaces
{
    public interface ITelegramBotCommandFactory
    {
        BotCommand GetBotCommand(long chatId, string query);
    }
}