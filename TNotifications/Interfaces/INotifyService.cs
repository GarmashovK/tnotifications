﻿using TNotifications.Domain.Messages;

namespace TNotifications.Interfaces
{
    public interface INotifyService
    {
        void PushMessage(IMessage message);
    }
}