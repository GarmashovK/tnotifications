﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CommonUtils.Common;
using CommonUtils.Common.Logger;
using CommonUtils.Helpers;
using TNotifications.Commands;
using TNotifications.Commands.Telegram;
using TNotifications.Interfaces;

namespace TNotifications.Factories
{
    public abstract class BaseTelegramBotCommandFactory : ITelegramBotCommandFactory
    {
        protected readonly INotifyService notifyService;
        protected readonly ILogger logger;
        protected readonly List<BaseTelegramBotCommand> commands;

        private string notFoundCommandName = "NotFoundCommand";

        protected BaseTelegramBotCommandFactory(
            INotifyService notifyService, 
            ILogger logger)
        {
            CodeContract.Requires<ArgumentNullException>(notifyService != null, nameof(notifyService));
            CodeContract.Requires<ArgumentNullException>(logger != null, nameof(logger));
            this.notifyService = notifyService;
            this.logger = logger;
            commands = GetTelegramCommands();
        }

        protected void UpdateCommands()
        {
            commands.AddRange(GetMainCommands());
            var helpCmd = commands.First(command => command.GetType() == typeof(HelpBotCommand)) as HelpBotCommand;
            helpCmd?.SetSupportedCommandsDescriptions(commands);
        }

        protected List<BaseTelegramBotCommand> GetTelegramCommands()
        {
            var initCommands = new List<BaseTelegramBotCommand>();
            initCommands.Add(GetHelpCommand());
            
            return initCommands;
        }

        protected virtual HelpBotCommand GetHelpCommand()
        {
            return new HelpBotCommand(notifyService, logger);
        }

        protected abstract IEnumerable<BaseTelegramBotCommand> GetMainCommands();

        protected virtual NotFoundCommand GetNotFoundCommand()
        {
            return new NotFoundCommand(notifyService, logger);
        }

        public virtual BotCommand GetBotCommand(long chatId, string query)
        {
            CodeContract.Requires<ArgumentNullException>(query.IsNotNullOrWhiteSpace(), nameof(query));
            logger.Info($"New telegram query [{query}] from chat with id [{chatId}]");
            var parseQuery = ParseQuery(query);

            var commandName = parseQuery.Item1;
            var parameters = parseQuery.Item2;

            var findCommand = commands.FirstOrDefault(c => c.CommandName == commandName) ?? GetNotFoundCommand();
            logger.Info($"Telegram command found: {findCommand.GetType()}");
            return findCommand.SetRequestValues(chatId, parameters);
        }

        protected Tuple<string, string> ParseQuery(string query)
        {
            CodeContract.Requires<ArgumentNullException>(query != null, nameof(query));
            var regex = new Regex(@"(?<command>/{1}[\w_]+)(@\S+)?\s*(?<parameters>.+)?");
            try
            {
                var match = regex.Match(query);
                if (match.Success == false)
                    return GetNotFoundParsedQuery(query);
                var commandName = match.Groups["command"].Value;
                var parameters = match.Groups["parameters"].Value.Trim();
                return Tuple.Create(commandName, parameters);
            }
            catch (Exception exc)
            {
                logger.Error("Ошибка при парсе команды бота!");
                logger.Error(exc.Message);
                return GetNotFoundParsedQuery(query);
            }
        }

        protected virtual Tuple<string, string> GetNotFoundParsedQuery(string query)
        {
            return Tuple.Create(notFoundCommandName, query);
        }
    }
}