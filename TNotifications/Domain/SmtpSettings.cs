﻿using System;
using System.Net;
using System.Net.Mail;
using CommonUtils.Common;

namespace TNotifications.Domain
{
    public class SmtpSettings
    {
        public string Username { get; private set; }
        public string Login { get; private set; }
        public string Password { get; private set; }
        public int Port { get; private set; }
        public string Host { get; private set; }

        public SmtpSettings(string host, int port, string username, string login, string password)
        {
            CodeContract.Requires<ArgumentNullException>(string.IsNullOrEmpty(host) == false, nameof(host));
            CodeContract.Requires<ArgumentException>(port > 0, nameof(port));
            CodeContract.Requires<ArgumentNullException>(string.IsNullOrEmpty(login) == false, nameof(login));
            CodeContract.Requires<ArgumentNullException>(string.IsNullOrEmpty(password) == false, nameof(password));
            Host = host;
            Port = port;
            Login = login;
            Password = password;
            Username = username;
        }

        public SmtpClient GetSmtpClient()
        {
            return new SmtpClient(Host, Port)
            {
                Credentials = new NetworkCredential(Login, Password) { },
                EnableSsl = true
            };
        }
    }
}
