﻿using System;
using CommonUtils.Common;
using CommonUtils.Helpers;

namespace TNotifications.Domain.Messages
{
    public class TelegramNotifyMessage : Message
    {
        public string TextMessage { get; private set; }

        public TelegramNotifyMessage(string textMsg)
        {
            CodeContract.Requires<ArgumentNullException>(textMsg.IsNotNullOrWhiteSpace(), nameof(textMsg));
            TextMessage = textMsg;
        }
    }
}