﻿using System;
using System.Text;
using CommonUtils.Common;
using CommonUtils.Helpers;

namespace TNotifications.Domain.Messages
{
    public class EmailMessage : Message
    {
        public string AddressTo { get; private set; }
        public string Subject { get; private set; }
        public string Body { get; private set; }

        public EmailMessage(string address, string subject, string body)
        {
            CodeContract.Requires<ArgumentNullException>(address.IsNotNullOrWhiteSpace(), nameof(address));
            CodeContract.Requires<ArgumentNullException>(subject.IsNotNullOrWhiteSpace(), nameof(subject));
            CodeContract.Requires<ArgumentNullException>(body.IsNotNullOrWhiteSpace()   , nameof(body));
            AddressTo = address;
            Subject = subject;
            Body = body;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();

            builder.AppendLine($"to: {AddressTo}");
            builder.AppendLine($"subject: {Subject}");
            builder.AppendLine($"body: {Body}");

            return builder.ToString();
        }
    }
}