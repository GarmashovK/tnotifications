﻿using System;
using CommonUtils.Common;
using CommonUtils.Helpers;

namespace TNotifications.Domain.Messages
{
    public class TelegramChatMessage : Message
    {
        public long ChatId { get; private set; }
        public string TextMessage { get; private set; }

        public TelegramChatMessage(long chatId, string textMessage)
        {
            CodeContract.Requires<ArgumentNullException>(chatId != 0, nameof(chatId));
            CodeContract.Requires<ArgumentNullException>(textMessage.IsNotNullOrWhiteSpace(), nameof(textMessage));
            ChatId = chatId;
            TextMessage = textMessage;
        }
    }
}