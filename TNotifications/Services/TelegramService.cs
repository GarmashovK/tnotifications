﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonUtils.Common;
using CommonUtils.Common.Logger;
using ConcurrentCollections;
using MoreLinq.Extensions;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using TNotifications.Interfaces;

namespace TNotifications.Services
{
    public class TelegramService : ITelegramService
    {
        private readonly ITelegramBotClient tBotClient;
        private readonly ILogger logger;
        protected readonly ConcurrentHashSet<long> SupportedChatIds = new ConcurrentHashSet<long>();
        
        public TelegramService(ITelegramBotClient tBotClient, ILogger logger)
        {
            CodeContract.Requires<ArgumentNullException>(tBotClient != null, nameof(tBotClient));
            CodeContract.Requires<ArgumentNullException>(logger != null, nameof(logger));
            this.tBotClient = tBotClient;
            this.logger = logger;
        }

        public void AddChat(long chatId)
        {
            if (SupportedChatIds.Contains(chatId) == false)
                SupportedChatIds.Add(chatId);
        }

        public bool SupportsChat(long chatId)
        {
            return SupportedChatIds.Contains(chatId);
        }

        public IReadOnlyList<long> GetSupportedChats()
        {
            return SupportedChatIds.ToList();
        }

        public void SendMessageToChat(long chatId, string message)
        {
            if (SupportedChatIds.Contains(chatId))
                SendMessage(chatId, message);
        }

        private void SendMessage(long chatId, string message)
        {
            tBotClient.SendTextMessageAsync(chatId, message, ParseMode.Markdown);
            var logMsg = GetLoggedMessage(chatId, message);
            logger.Info(logMsg);
        }

        public void SendMessageToAllChats(string message)
        {
            SupportedChatIds.ForEach(chatId => { SendMessage(chatId, message); });
        }

        private string GetLoggedMessage(long chatId, string message)
        {
            var strBuilder = new StringBuilder();

            strBuilder.AppendLine($"{typeof(TelegramService): sended message}");
            strBuilder.AppendLine($"chatId: {chatId}, message: {message}");

            return strBuilder.ToString();
        }
    }
}