﻿using System;
using System.Net.Mail;
using System.Text;
using CommonUtils.Common;
using CommonUtils.Common.Logger;
using CommonUtils.Common.Results;
using TNotifications.Domain;
using TNotifications.Domain.Messages;
using TNotifications.Interfaces;

namespace TNotifications.Services
{
    public class EmailService : IEmailService
    {
        private readonly SmtpSettings smtpSettings;
        private readonly ILogger logger;

        public EmailService(SmtpSettings smtpSettings, ILogger logger)
        {
            CodeContract.Requires<ArgumentNullException>(smtpSettings != null, nameof(smtpSettings));
            CodeContract.Requires<ArgumentNullException>(logger != null, nameof(logger));
            this.smtpSettings = smtpSettings;
            this.logger = logger;
        }

        public Result SendEmailMessage(EmailMessage emailMessage)
        {
            var smtpClient = smtpSettings.GetSmtpClient();
            
            try
            {
                smtpClient.Send(new MailMessage(
                    smtpSettings.Username,
                    emailMessage.AddressTo,
                    emailMessage.Subject,
                    emailMessage.Body)
                {
                    IsBodyHtml = true
                });
                logger.Info(GetSuccessSendMessageInfo(emailMessage));
                return Result.Success();
            }
            catch (Exception exc)
            {
                logger.Error(exc.Message);
                return Result.Error(exc.Message);
            }
        }

        private string GetSuccessSendMessageInfo(EmailMessage emailMessage)
        {
            var builder = new StringBuilder();

            builder.AppendLine("Email sended!");
            builder.AppendLine($"from: {smtpSettings.Login}");
            builder.AppendLine(emailMessage.ToString());

            return builder.ToString();
        }
    }
}