﻿using System;
using System.Collections.Concurrent;
using CommonUtils.Common;
using CommonUtils.Common.Logger;
using TNotifications.Domain.Messages;
using TNotifications.Interfaces;

namespace TNotifications.Services
{
    public class NotifyService : INotifyService
    {
        private readonly IEmailService emailService;
        private readonly ITelegramService telegramService;
        private readonly ILogger logger;

        public NotifyService(
            IEmailService emailService, 
            ITelegramService telegramService,
            ILogger logger)
        {
            CodeContract.Requires<ArgumentNullException>(logger != null, nameof(logger));
            
            this.emailService = emailService;
            this.telegramService = telegramService;
            this.logger = logger;
        }

        public void PushMessage(IMessage message)
        {
            if (message == null)
            {
                logger.Error("TRY NOTIFY EMPTY MESSAGE.");
                return;
            }
            switch (message)
            {
                case TelegramNotifyMessage telegramNotifyMessage:
                    logger.Info("Sended telegram notify message!");
                    telegramService?.SendMessageToAllChats(telegramNotifyMessage.TextMessage);
                    break;
                case TelegramChatMessage telegramChatMessage:
                    logger.Info("Sended telegram chat message!");
                    telegramService?.SendMessageToChat(telegramChatMessage.ChatId, telegramChatMessage.TextMessage);
                    break;
                case EmailMessage emailMessage:
                    logger.Info("Sended email message!");
                    emailService?.SendEmailMessage(emailMessage);
                    break;
            }
        }
    }
}