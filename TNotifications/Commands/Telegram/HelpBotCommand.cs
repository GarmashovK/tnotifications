﻿using System.Collections.Generic;
using System.Text;
using CommonUtils.Common.Logger;
using MoreLinq;
using TNotifications.Domain.Messages;
using TNotifications.Interfaces;

namespace TNotifications.Commands.Telegram
{
    public class HelpBotCommand : BaseTelegramBotCommand
    {
        protected List<string> CommandsDescriptions = new List<string>();

        public HelpBotCommand(INotifyService notifyService, ILogger logger)
            : base("/help", notifyService, logger)
        {
        }

        protected override string GetDescription()
        {
            return "- вывести список команд";
        }

        protected override void CustomOperation()
        {
            SendTelegramMessage(ChatId, GetHelp());
        }
        
        protected string GetHelp()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Поддерживаемые команды:");

            CommandsDescriptions.ForEach(commandDesc => sb.AppendLine(commandDesc));

            return sb.ToString();
        }

        public HelpBotCommand SetSupportedCommandsDescriptions(IEnumerable<BaseTelegramBotCommand> supportedCommands)
        {
            CommandsDescriptions.Clear();
            supportedCommands.ForEach(supportedCommand =>
                CommandsDescriptions.Add(supportedCommand.Description));
            return this;
        }
    }
}