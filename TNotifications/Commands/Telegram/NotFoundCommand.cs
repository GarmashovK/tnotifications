﻿using CommonUtils.Common.Logger;
using TNotifications.Domain.Messages;
using TNotifications.Interfaces;

namespace TNotifications.Commands.Telegram
{
    public class NotFoundCommand : BaseTelegramBotCommand
    {
        public NotFoundCommand(INotifyService notifyService, ILogger logger) 
            : base("NotFoundCommand", notifyService, logger)
        {
        }


        protected override string GetDescription()
        {
            return "Not found command";
        }

        protected override void CustomOperation()
        {
            //notifyService.PushMessage(new TelegramChatMessage(ChatId, $"Неизвестная команда: {Parameters}."));
        }
    }
}