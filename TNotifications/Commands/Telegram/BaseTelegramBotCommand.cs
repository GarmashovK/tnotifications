﻿using System;
using System.Linq;
using CommonUtils.Common;
using CommonUtils.Common.Logger;
using CommonUtils.Common.Results;
using CommonUtils.Helpers;
using TNotifications.Domain.Messages;
using TNotifications.Interfaces;

namespace TNotifications.Commands.Telegram
{
    public abstract class BaseTelegramBotCommand : BotCommand
    {
        public long ChatId { get; private set; }
        public string Parameters { get; private set; }

        public string Description => $"{CommandName} {GetDescription()}";

        protected readonly INotifyService notifyService;
        protected readonly ILogger logger;

        protected BaseTelegramBotCommand(
            string commandName, 
            INotifyService notifyService,
            ILogger logger)
        {
            CodeContract.Requires<ArgumentNullException>(commandName.IsNotNullOrWhiteSpace(), nameof(commandName));
            CodeContract.Requires<ArgumentNullException>(notifyService != null, nameof(notifyService));
            CodeContract.Requires<ArgumentNullException>(logger != null, nameof(logger));
            this.notifyService = notifyService;
            this.logger = logger;
            CommandName = commandName;
            CommandAction = DoOperation;
        }

        protected virtual string GetDescription() => "без описания";

        public BaseTelegramBotCommand SetRequestValues(long chatId, string parameters)
        {
            ChatId = chatId;
            Parameters = parameters;
            ParseParameters(Parameters);
            return this;
        }

        protected void SafeAction(Action action)
        {
            try
            {
                action.Invoke();
            }
            catch (Exception exc)
            {
                logger.Error(exc.Message);
                SendTelegramMessage($"Ошибка при выполнении команды {CommandName}");
            }
        }

        protected string[] GetSplittedParameters()
        {
            return Parameters.Split(' ').Where(p => p.IsNotNullOrWhiteSpace()).ToArray();
        }

        protected void SendTelegramMessage(string message)
        {
            notifyService.PushMessage(new TelegramChatMessage(ChatId, message));
        }

        protected void SendTelegramMessage(long chatId, string message)
        {
            notifyService.PushMessage(new TelegramChatMessage(chatId, message));
        }
        
        protected Result<T> TryParseParameters<T>(Func<Result<T>> parseFunc)
        {
            try
            {
                return parseFunc.Invoke();
            }
            catch (Exception exc)
            {
                logger.Error($"Ошибка при попытке спарсить параметры из телеграмма {Parameters}");
                logger.Error(exc.Message);
                return Result.Error<T>("Неверные параметры!");
            }
        }

        protected void DoOperation()
        {
            try
            {
                logger.Info($"Try do command: {CommandName}");
                CustomOperation();
            }
            catch (Exception exc)
            {
                logger.Error($"Telegram [{CommandName}] operation error. Exception: {exc.Message}");
                notifyService.PushMessage(new TelegramChatMessage(ChatId, "Ошибка при обработке запроса."));
            }
        }

        protected virtual void CustomOperation() {}

        protected virtual void ParseParameters(string parameters) { }
        
        public virtual BotCommand GetBotCommand()
        {
            return new BotCommand(CommandName, DoOperation);
        }
    }
}