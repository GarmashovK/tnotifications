﻿using System;
using CommonUtils.Common;

namespace TNotifications.Commands
{
    public class BotCommand
    {
        public string CommandName { get; protected set; }
        protected Action CommandAction;

        protected BotCommand() { }

        public BotCommand(string commandName, Action command)
        {
            CodeContract.Requires<ArgumentNullException>(string.IsNullOrWhiteSpace(commandName) == false, nameof(commandName));
            CodeContract.Requires<ArgumentNullException>(command != null, nameof(command));
            CommandName = commandName;
            CommandAction = command;
        }

        public void Execute()
        {
            CommandAction.Invoke();
        }
    }
}
