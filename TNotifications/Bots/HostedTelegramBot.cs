﻿using System;
using System.Linq;
using CommonUtils.Common;
using CommonUtils.Common.Extensions;
using CommonUtils.Common.Logger;
using CommonUtils.Helpers;
using MoreLinq;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Args;
using TNotifications.Interfaces;

namespace TNotifications.Bots
{
    public class HostedTelegramBot : IHostedTelegramBot
    {
        private readonly ILogger logger;

        protected ITelegramBotClient Client;
        private readonly ITelegramService telegramService;
        private readonly ITelegramBotCommandFactory commandFactory;

        public HostedTelegramBot(
            ITelegramBotClient client,
            ITelegramService telegramService,
            ITelegramBotCommandFactory commandFactory,
            ILogger logger)
        {
            CodeContract.Requires<ArgumentNullException>(client != null, nameof(client));
            CodeContract.Requires<ArgumentNullException>(telegramService != null, nameof(telegramService));
            CodeContract.Requires<ArgumentNullException>(commandFactory != null, nameof(commandFactory));
            CodeContract.Requires<ArgumentNullException>(logger != null, nameof(logger));
            this.commandFactory = commandFactory;
            this.logger = logger;
            Client = client;
            this.telegramService = telegramService;
        }
        
        public void StartReceiving()
        {
            Client.OnUpdate += Client_OnUpdate;
            Client.StartReceiving();
        }

        public void StopReceiving()
        {
            Client.OnUpdate -= Client_OnUpdate;
            Client.StopReceiving();
        }

        private void Client_OnUpdate(object sender, UpdateEventArgs e)
        {
            logger.Info("Chat message!");
            logger.Info($"{JsonConvert.SerializeObject(e.Update, Formatting.Indented)}");
            if (e.Update.CallbackQuery != null || e.Update.InlineQuery != null)
            {
                return;
            }

            var message = e.Update.Message;
            if (string.IsNullOrWhiteSpace(message?.Text) 
                || message.Text.StartsWith('/') == false)
            {
                logger.Error("Telegram message is denied!");
                return;
            }

            var chatId = message.Chat.Id;
            
            logger.Info($"Telegram query: ChatId {chatId}, Message: {message.Text}");
            Safe.Do(logger, () => { DoCommand(chatId, message.Text); });
        }

        protected void DoCommand(long chatId, string text)
        {
            if (string.IsNullOrWhiteSpace(text)) return;
            if (telegramService.SupportsChat(chatId) == false)
                return;

            var queries = text.Split('\n').Where(t => t.IsNotNullOrWhiteSpace());
            queries.ForEach(query =>
            {
                DoQuery(chatId, query);
            });
        }

        private void DoQuery(long chatId, string query)
        {
            var command = commandFactory.GetBotCommand(chatId, query);
            if (command == null)
                logger.Info($"Empty command for telegram query {query}");
            else
            {
                logger.Info($"Execute command: {command.GetType()}");
                command.Execute();
            }
        }
    }
}